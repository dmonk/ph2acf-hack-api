FROM gitlab-registry.cern.ch/mmagheri/ph-2-acf-be-sw
RUN rm -rf /home/cmsTkUser

WORKDIR /
COPY ./requirements.txt /requirements.txt
RUN python3 -m pip install -r requirements.txt

COPY ./app /app
WORKDIR /app
CMD ["bash", "run.sh"]