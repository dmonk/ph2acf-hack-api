from typing import Union
from fastapi import FastAPI

import subprocess


app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/startup")
def configure():
    subprocess.run("cd /ph-2-acf-be-sw && source setup.sh && ot_module_test -f /home/cmx/dmonk/cosmic_rack_oldSettings.xml -b --startUp", shell=True)
    return {"status": "complete"}