export LD_LIBRARY_PATH=/opt/cactus/lib:/opt/smash/lib:$LD_LIBRARY_PATH
export PATH=/opt/smash/bin:/opt/cactus/bin/emp:/opt/cactus/bin/serenity:{$HOME}/bin:$PATH
export SMASH_PATH=/opt/smash
export SMASH_LOGLEVEL=INFO
export SMASH_DEFAULT_CONFIG=/home/cmx/board/smash_config/default.smash
export SERENITY_CONFIG=/home/cmx/board/serenity.yml
export UHAL_ENABLE_IPBUS_PCIE=
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

uvicorn main:app --host 0.0.0.0 --port 8000
